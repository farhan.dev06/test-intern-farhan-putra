async function tesAPI() {
  try {
    const config = {
      params: { page: 1, append: 'small_image', sort: 'published_at' },
    };
    const endpoint = await axios.get(
      'https://suitmedia-backend.suitdev.com/api/ideas',
      config
    );
    const showData = endpoint.data;
    console.log(showData);
  } catch (err) {
    console.log(err);
  }
}
