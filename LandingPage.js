const selection = document.querySelector('.show');
const header = document.querySelector('.header');
let showAmount = +selection.options[selection.selectedIndex].value;
const cardContainer = document.getElementsByClassName('card-container');
const showAmountText = document.querySelector('.p_show');

let lastScrollUp = window.scrollY;

window.addEventListener('scroll', () => {
  if (lastScrollUp < window.scrollY) {
    header.classList.add('nav-hidden');
  } else {
    header.classList.remove('nav-hidden');
  }
  lastScrollUp = window.scrollY;
});

const cardData = {
  judul: 'Kenali Tingkat Influencers berdasarkan Jumlah Followers',
  tanggal: '5 SEPTEMBER 2022',
  image: 'Phone_socialMedia.avif',
};

function addCard(data) {
  const card = document.createElement('div');
  card.classList.add('card');
  card.style.width = '18rem';
  card.style.marginBottom = '20px';

  const img = document.createElement('img');
  img.classList.add('card-img-top');
  img.src = data.image;
  card.appendChild(img);

  const body = document.createElement('div');
  body.classList.add('card-body');

  const subtitle = document.createElement('h6');
  subtitle.classList.add('card-subtitle', 'mb-2', 'text-body-secondary');
  subtitle.textContent = data.tanggal;
  body.appendChild(subtitle);

  const judul = document.createElement('h5');
  judul.classList.add('card-title');
  judul.textContent = data.judul;
  body.appendChild(judul);
  card.appendChild(body);
  return card;
}

if (showAmount == 10) {
  showAmountText.textContent = 'Showing 1-10 of 100';
  for (let i = 0; i < showAmount; i++) {
    const card = addCard(cardData);
    cardContainer[0].appendChild(card);
  }
}

selection.addEventListener('change', () => {
  cardContainer[0].innerHTML = '';
  showAmount = +selection.options[selection.selectedIndex].value;
  if (showAmount == 20) {
    showAmountText.textContent = 'Showing 1-20 of 100';
  } else if (showAmount == 50) {
    showAmountText.textContent = 'Showing 1-50 of 100';
  } else {
    showAmountText.textContent = 'Showing 1-10 of 100';
  }
  for (let i = 0; i < showAmount; i++) {
    const card = addCard(cardData);
    cardContainer[0].appendChild(card);
  }
});
